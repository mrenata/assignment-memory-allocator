#include <stdio.h>
#include "tests.h"

int main(){
    test1("\n 1 Usual memory allocation \n");
    test2("\n 2 Free one block from several \n");
    test3("\n 3 Free two blocks from several \n");
    test4("\n 4 New region of memory expands the old \n");
    test5("\n 5 New memory region is allocated in a new place \n");
    
    return 0;
}
