#ifndef _TESTS_
#define _TESTS_

/* Обычное успешное выделение памяти */
void test1(char const* testName);

/* Освобождение одного блока из нескольких выделенных */
void test2(char const* testName);

/* Освобождение двух блоков из нескольких выделенных */
void test3(char const* testName);

/* Память закончилась, новый регион памяти расширяет старый */
void test4(char const* testName);

/* Память закончилась, старый регион памяти не расширить
 * из-за другого выделенного диапазона адресов,
 * новый регион выделяется в другом месте */
void test5(char const* testName);
#endif
