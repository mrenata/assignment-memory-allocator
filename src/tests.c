#include <stdio.h>
#include "tests.h"
#include "mem.h"
#include "mem_debug.h"

void test1(char const* testName){
    printf("%s\n", testName);
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t * b = _malloc(1050);
    *b = 1000;     
    debug_heap(stdout, heap);
}

void test2(char const* testName){
    printf("%s\n", testName);
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* b1 = _malloc(1000);
    uint16_t* b2 = _malloc(2000);
    uint16_t* b3 = _malloc(3000);
    *b1 = 150;
    *b2 = 10;
    *b3 = 100;   
    debug_heap(stdout, heap);
    _free(b2);
    debug_heap(stdout, heap);
}

void test3(char const* testName){
    printf("%s\n", testName);
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* b1 = _malloc(1000);
    uint16_t* b2 = _malloc(2000);
    uint16_t* b3 = _malloc(3000);
    *b1 = 150;
    *b2 = 10;
    *b3 = 100;   
    debug_heap(stdout, heap);
    _free(b2);
    debug_heap(stdout, heap);
    _free(b1);
    debug_heap(stdout, heap);
}

void test4(char const* testName){
    printf("%s\n", testName);
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* b1 = _malloc(65536);
    uint16_t* b2 = _malloc(101);
    *b1 = 1000;
    debug_heap(stdout, heap);
    *b2 = 11001;
    debug_heap(stdout, heap);
    
}

void test5(char const* testName){
    printf("%s\n", testName);
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* b1 = _malloc(65530*2);
    uint16_t* b2 = _malloc(100);
    *b1 = 1000;
    debug_heap(stdout, heap);
    *b2 = 11001;
    debug_heap(stdout, heap);
}
